import serial
from time import sleep
import time

port = "/dev/ttyACM0"
ser = serial.Serial(port, 38400, timeout=0)
while True:
    ser.write('A')
    data = ser.read(10)
    if len(data) > 1:
        print 'got data: ', (ord(data[0])*256 + ord(data[1]))
    sleep(1.0)
ser.close()
