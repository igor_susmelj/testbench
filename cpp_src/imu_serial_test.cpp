#include <SerialStream.h>
#include <iostream>
#include <stdint.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <time.h>

#include "data_interface_slave.h"

int main(int argc, char **argv)
{


	//setup serial stream with LibSerial
	using namespace LibSerial;
	SerialStream serial_port;
	serial_port.Open("/dev/ttyACM0");
	
	Slave_Data slave_data1{0,0,0,0,0,0,0,0,0,0,0,0};

	//byte to send to slave
	char test = 'A';

	//initialize clock for timing
	clock_t t;

	std::cout << "Initializing testbench..." << std::endl;
	sleep(1);
	std::cout << "IMU1_AX | IMU1_AY | IMU1_AZ | IMU1_GX | IMU1_GY | IMU1_GZ" << std::endl;

	while(true)
	{
		t = clock();

		//write byte to slave
		serial_port.write(&test, 1);
		
		//loop until data ready
		while (serial_port.rdbuf()->in_avail() > 0)
		{
			//buffer for 12 bytes -> 6 int16_t values from the IMU
			char input_buff[24];

			//read data into buffer
			serial_port.read(input_buff, 24);
		
			//process data (convert bytes into int16_t)
			int16_t* data_tmp = &(slave_data1.IMU1_AX);
			for(int i = 0; i < 12; ++i)
			{
				data_tmp[i] = ((uint8_t)input_buff[2*i+1] << 8)  | (uint8_t)input_buff[2*i];
			}
			//stop cloc
			t = clock() - t;

			//output values from IMU
			std::cout << slave_data1.IMU1_AX << " : " << slave_data1.IMU1_AY << " : " << slave_data1.IMU1_AZ << " : "  <<slave_data1.IMU1_GX << " : "  <<slave_data1.IMU1_GY << " : "  <<slave_data1.IMU1_GZ << std::endl;


			std::cout << slave_data1.IMU2_AX << " : " << slave_data1.IMU2_AY << " : " << slave_data1.IMU2_AZ << " : "  <<slave_data1.IMU2_GX << " : "  <<slave_data1.IMU2_GY << " : "  <<slave_data1.IMU2_GZ << std::endl;
			//report timing
			std::cout << "time took from master(1B)->slave(12B)->master (13B): " << ((float)t)/CLOCKS_PER_SEC << std::endl;
	
		}
		std::cout << "----------" << std::endl;		
		sleep(1);
	}

	//close serial port
	serial_port.Close();
	
	return 0;
}
