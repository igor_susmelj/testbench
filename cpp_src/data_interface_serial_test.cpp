#include <SerialStream.h>
#include <iostream>
#include <stdint.h>
#include <unistd.h>
#include <cstdlib>
#include <string>
#include <ctime>

#include "data_interface_slave.h"

int main(int argc, char **argv)
{


	//setup serial stream with LibSerial
	using namespace LibSerial;
	SerialStream serial_port;
	serial_port.Open("/dev/ttyACM0");

	//serial_port.SetBaudRate(SerialStreamBuf::BAUD_115200);
	serial_port.SetCharSize(SerialStreamBuf::CHAR_SIZE_8);
	serial_port.SetNumOfStopBits(1);
	serial_port.SetParity(SerialStreamBuf::PARITY_NONE);
	serial_port.SetFlowControl(SerialStreamBuf::FLOW_CONTROL_HARD);
	
	Master_Data master_data = {
		1,
		{1,2,3,4,5},
		{6,7,8,9,10},
		44,
		79,
		23,
		99,
		11,
		383
	};	


	Slave_Data slave_data;

	Master_Data master_data_ret = {
		0,
		{0,0,0,0,0},
		{0,0,0,0,0},
		0,
		0,
		0,
		0,
		0,
		0
	};	



	//initialize clock for timing
	clock_t t;

	std::cout << "Initializing testbench..." << std::endl;
	sleep(2);

	std::cout << "Size of master package: " << sizeof(Master_Data) << " Bytes" << std::endl;
	std::cout << "Size of slave package: " << sizeof(Slave_Data) << " Bytes"<< std::endl;
		

	while(true)
	{
		t = clock();

		serial_port.write((const char *)&master_data, sizeof(Master_Data));	
		usleep(100);
	
		while (serial_port.rdbuf()->in_avail() > 0)
		{
			serial_port.read((char *)&slave_data, sizeof(Slave_Data));
	
			std::cout << "data found: " << (int) slave_data.ID << " " << (int)slave_data.IMU1_AY << " " << (int)slave_data.SOFT_POT_3 << " " << (int) slave_data.MCU1_RPM << std::endl;	

				
		}
	

		t = clock() - t;
		float transfer_rate = (sizeof(Master_Data) + sizeof(Slave_Data))/((float)t/CLOCKS_PER_SEC)/1024; 
		std::cout << "Time of full transfer: " << ((float)t/CLOCKS_PER_SEC) << " or " << transfer_rate << " kBytes/s" << std::endl;

		std::cout << "----------" << std::endl;		
		usleep(50 * 1000);
	}

	//close serial port
	serial_port.Close();
	
	return 0;
}
