all: imu_serial_test data_interface_serial_test
	
imu_serial_test: cpp_src/imu_serial_test.cpp
	g++ -Wall -O3 -o imu_serial_test cpp_src/imu_serial_test.cpp -lserial

data_interface_serial_test: cpp_src/data_interface_serial_test.cpp
	g++ -Wall -O3 -o data_interface_serial_test cpp_src/data_interface_serial_test.cpp -lserial

debug_interface: cpp_src/data_interface_serial_test.cpp
	g++ -Wall -O3 -g -o data_interface_serial_test cpp_src/data_interface_serial_test.cpp -lserial

clean:
	rm -f imu_serial_test data_interface_serial_test
